package com.example.panqu.proyectoiot.Models;

/**
 * Created by panqu on 22/05/2018.
 */

public class DispositivosModel {

    private Integer idDispositivo;
    private String Nombre;
    private String Status;

    public DispositivosModel(Integer idDispositivo, String nombre, String status) {
        this.idDispositivo = idDispositivo;
        Nombre = nombre;
        Status = status;
    }

    public Integer getIdDispositivo() {
        return idDispositivo;
    }

    public void setIdDispositivo(Integer idDispositivo) {
        this.idDispositivo = idDispositivo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
