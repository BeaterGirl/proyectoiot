package com.example.panqu.proyectoiot.Models;

/**
 * Created by panqu on 22/05/2018.
 */

public class ConsumoModel {

    private Double Energia;
    private String fecha;
    private String nombre;

    public ConsumoModel(Double energia, String fecha, String nombre) {
        Energia = energia;
        this.fecha = fecha;
        this.nombre = nombre;
    }

    public Double getEnergia() {
        return Energia;
    }

    public void setEnergia(Double energia) {
        Energia = energia;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}