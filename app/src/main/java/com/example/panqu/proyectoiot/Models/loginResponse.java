package com.example.panqu.proyectoiot.Models;

/**
 * Created by panqu on 30/05/2018.
 */

public class loginResponse {

    String ok;

    public loginResponse(String ok) {
        this.ok = ok;
    }

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }
}
