package com.example.panqu.proyectoiot.Activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.panqu.proyectoiot.Adapters.DispositivosAdapter;
import com.example.panqu.proyectoiot.Models.DispositivosModel;
import com.example.panqu.proyectoiot.Models.StatusResponse;
import com.example.panqu.proyectoiot.R;
import com.example.panqu.proyectoiot.Servicios;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DispositivosActivity extends AppCompatActivity implements DispositivosAdapter.onRadioButton {

    private List<DispositivosModel> listaDispositivos = new ArrayList<>();
    DispositivosAdapter dispositivosAdapter;
    RecyclerView recyclerViewDispositivos;

    RadioGroup rgStatus;
    RadioButton rbOn;
    RadioButton rbOff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispositivos);
        rgStatus = findViewById(R.id.rgStatus);

        recyclerViewDispositivos = findViewById(R.id.rvDispositivos);
        //rbOn = findViewById(R.id.rbOn);
        //rbOff = findViewById(R.id.rbOff);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Dispositivos");

        getCallRetrofit();
    }

    private void getCallRetrofit() {
        String ENDPOINT = getString(R.string.host);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ENDPOINT).addConverterFactory(GsonConverterFactory.create()).build();

        Servicios servicios = retrofit.create(Servicios.class);
        servicios.getDispositivos().enqueue(new Callback<List<DispositivosModel>>() {
            @Override
            public void onResponse(Call<List<DispositivosModel>> call, Response<List<DispositivosModel>> response) {
                Log.e("", response.toString());
                listaDispositivos = response.body();

                startAdapter();
            }

            @Override
            public void onFailure(Call<List<DispositivosModel>> call, Throwable t) {
                Log.e("", "onFailure");
            }
        });
    }

    public void startAdapter() {
        dispositivosAdapter = new DispositivosAdapter(getApplicationContext(), listaDispositivos, this);
        recyclerViewDispositivos.setAdapter(dispositivosAdapter);
        LinearLayoutManager linearLayoutManager = (new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerViewDispositivos.setLayoutManager(linearLayoutManager);
        recyclerViewDispositivos.hasFixedSize();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClickRadio(int opcion) {
        switch (opcion) {
            case 1: {
                getCallRetrofitOn();
                break;
            }
            case 2: {
                getCallRetrofitOff();
                break;
            }
        }
    }

    private void getCallRetrofitOn() {
        String ENDPOINT = getString(R.string.host);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ENDPOINT).addConverterFactory(GsonConverterFactory.create()).build();
        String status = "1";
        Servicios servicios = retrofit.create(Servicios.class);
        servicios.getStatus(status).enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                String i = response.body().getStatus();
                Toast.makeText(DispositivosActivity.this, i, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {
                Log.e("", "onFailure");
            }
        });
    }

    private void getCallRetrofitOff() {
        String ENDPOINT = getString(R.string.host);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ENDPOINT).addConverterFactory(GsonConverterFactory.create()).build();
        String status = "0";
        Servicios servicios = retrofit.create(Servicios.class);
        servicios.getStatus(status).enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                String i = response.body().getStatus();
                Toast.makeText(DispositivosActivity.this, i, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {
                Log.e("", "onFailure");
            }
        });
    }


}
