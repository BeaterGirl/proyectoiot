package com.example.panqu.proyectoiot.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.panqu.proyectoiot.Models.DispositivosModel;
import com.example.panqu.proyectoiot.Models.loginResponse;
import com.example.panqu.proyectoiot.R;
import com.example.panqu.proyectoiot.Servicios;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    Button buttonLogin;

    EditText userName;
    EditText pass;

    TextView tvError;

    String userNameText;
    String passText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonLogin = findViewById(R.id.btnLogin);
        userName = findViewById(R.id.etUserName);
        pass = findViewById(R.id.etPassword);
        tvError = findViewById(R.id.tvError);


        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCallRetrofit();
            }
        });

    }

    private void getCallRetrofit() {
        String ENDPOINT = getString(R.string.host);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ENDPOINT).addConverterFactory(GsonConverterFactory.create()).build();


        userNameText = userName.getText().toString();
        passText = pass.getText().toString();

        Servicios servicios = retrofit.create(Servicios.class);
        servicios.getLogin(userNameText, passText).enqueue(new Callback<loginResponse>() {
            @Override
            public void onResponse(Call<loginResponse> call, Response<loginResponse> response) {
                String msg = response.body().getOk();
                if (msg.equalsIgnoreCase("bien")) {
                    Intent intent = new Intent(MainActivity.this, MenuActivity.class);
                    startActivity(intent);
                }else{
                    userName.setText("");
                    pass.setText("");
                    tvError.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<loginResponse> call, Throwable t) {
                Log.e("", "onFailure");
            }
        });
    }

}
