package com.example.panqu.proyectoiot;

import com.example.panqu.proyectoiot.Models.ConsumoModel;
import com.example.panqu.proyectoiot.Models.DispositivosModel;
import com.example.panqu.proyectoiot.Models.StatusResponse;
import com.example.panqu.proyectoiot.Models.loginResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by panqu on 28/05/2018.
 */

public interface Servicios {

    @GET("consulta.php")
    Call<List<ConsumoModel>> getConsumo();

    @GET("consultaDispositivo.php")
    Call<List<DispositivosModel>> getDispositivos();

    @FormUrlEncoded
    @POST("prenderApagar.php")
    Call<StatusResponse> getStatus(@Field("opcion") String opcion);


    @FormUrlEncoded
    @POST("iniciarSesion.php")
    Call<loginResponse> getLogin(@Field("userName") String userName,
                                 @Field("pass") String pass);


}
