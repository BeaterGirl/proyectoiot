package com.example.panqu.proyectoiot.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.panqu.proyectoiot.R;

public class MenuActivity extends AppCompatActivity {

    Button btnConsulta;
    Button btnDispositivo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        initControls();
        goToActivities();
    }

    private void initControls() {
        btnConsulta = findViewById(R.id.btnConsulta);
        btnDispositivo = findViewById(R.id.btnDispositivos);
    }

    private void goToActivities() {
        btnConsulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, ConsultaActivity.class);
                startActivity(intent);
            }
        });

        btnDispositivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, DispositivosActivity.class);
                startActivity(intent);
            }
        });
    }
}
