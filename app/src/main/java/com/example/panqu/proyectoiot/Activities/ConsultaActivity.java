package com.example.panqu.proyectoiot.Activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.example.panqu.proyectoiot.Adapters.ConsumoAdapter;
import com.example.panqu.proyectoiot.Models.ConsumoModel;
import com.example.panqu.proyectoiot.R;
import com.example.panqu.proyectoiot.Servicios;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConsultaActivity extends AppCompatActivity {

    private List<ConsumoModel> listaConsumo = new ArrayList<>();
    private ConsumoAdapter adapter;
    private RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta);
        recyclerView = findViewById(R.id.rvConsulta);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Consumo de Energia");

        getCallRetrofit();
    }

    private void getCallRetrofit() {
        String ENDPOINT = getString(R.string.host);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ENDPOINT).addConverterFactory(GsonConverterFactory.create()).build();

        Servicios servicios = retrofit.create(Servicios.class);
        servicios.getConsumo().enqueue(new Callback<List<ConsumoModel>>() {
            @Override
            public void onResponse(Call<List<ConsumoModel>> call, Response<List<ConsumoModel>> response) {
                listaConsumo = response.body();
                startAdapter();
            }

            @Override
            public void onFailure(Call<List<ConsumoModel>> call, Throwable t) {
                Log.e("", "onFailure");
            }
        });
    }

    public void startAdapter() {
        adapter = new ConsumoAdapter(this, listaConsumo);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.hasFixedSize();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }
}
