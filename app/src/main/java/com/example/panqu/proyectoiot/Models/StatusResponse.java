package com.example.panqu.proyectoiot.Models;

/**
 * Created by panqu on 29/05/2018.
 */

public class StatusResponse {

    String status;

    public StatusResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
