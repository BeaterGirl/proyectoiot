package com.example.panqu.proyectoiot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.panqu.proyectoiot.Models.ConsumoModel;
import com.example.panqu.proyectoiot.R;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Created by panqu on 22/05/2018.
 */

public class ConsumoAdapter extends RecyclerView.Adapter<ConsumoAdapter.ItemViewHolder> {

    private Context mContext;
    private List<ConsumoModel> mList;

    public ConsumoAdapter(Context mContext, List<ConsumoModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_consumo, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ConsumoAdapter.ItemViewHolder holder, int position) {
       ConsumoModel model = mList.get(position);

        holder.tvDispositivo.setText(model.getNombre());
        holder.tvConsumo.setText(model.getEnergia().toString());

        String date1 = model.getFecha();
        SimpleDateFormat parseador = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy HH:mm");

        try {
            Date date = parseador.parse(date1);
            holder.tvFecha.setText(formateador.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView tvDispositivo;
        TextView tvFecha;
        TextView tvConsumo;

        public ItemViewHolder(View itemView) {
            super(itemView);
            tvDispositivo = itemView.findViewById(R.id.tvDispositivo);
            tvFecha = itemView.findViewById(R.id.tvFecha);
            tvConsumo = itemView.findViewById(R.id.tvConsumo);
        }
    }
}
