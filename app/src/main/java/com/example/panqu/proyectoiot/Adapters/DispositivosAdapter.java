package com.example.panqu.proyectoiot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.panqu.proyectoiot.Models.DispositivosModel;
import com.example.panqu.proyectoiot.R;

import java.util.List;

/**
 * Created by panqu on 22/05/2018.
 */

public class DispositivosAdapter extends RecyclerView.Adapter<DispositivosAdapter.ItemViewHolder> {

    private Context context;
    private List<DispositivosModel> dispositivosModelList;
    private onRadioButton mListener;

    public DispositivosAdapter(Context context, List<DispositivosModel> dispositivosModelList, onRadioButton mListener) {
        this.context = context;
        this.dispositivosModelList = dispositivosModelList;
        this.mListener = mListener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dispositivo, parent, false);
        return new DispositivosAdapter.ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DispositivosAdapter.ItemViewHolder holder, int position) {
        DispositivosModel model = dispositivosModelList.get(position);
        holder.tvNombreDispositivoItem.setText(model.getNombre());
        holder.ivIcon.setImageResource(R.drawable.ic_florist);

        String estado = model.getStatus();

        if (estado.equalsIgnoreCase("Prendido")) {
            holder.rbOn.setChecked(true);
        }else{
            holder.rbOff.setChecked(true);
        }

        holder.rbOn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                int opcion = 1;
                mListener.onClickRadio(opcion);
            }
        });

        holder.rbOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int opcion = 2;
                mListener.onClickRadio(opcion);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dispositivosModelList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView tvNombreDispositivoItem;
        ImageView ivIcon;
        RadioButton rbOn;
        RadioButton rbOff;

        public ItemViewHolder(View itemView) {
            super(itemView);
            tvNombreDispositivoItem = itemView.findViewById(R.id.tvDispositivoItem);
            ivIcon = itemView.findViewById(R.id.icon_image_view);
            rbOn = itemView.findViewById(R.id.rbOn);
            rbOff = itemView.findViewById(R.id.rbOff);
        }
    }

    public interface onRadioButton{
        void onClickRadio(int opcion);
    }
}
